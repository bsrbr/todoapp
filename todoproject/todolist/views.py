from django.http import Http404
from django.shortcuts import get_object_or_404, render

from .models import *

# Create your views here.

from django.http import HttpResponse
from django.template import loader


def index(request):
    todos = Todo.objects.all()
    context = {'todos':todos}
    return render(request, 'todolist/index.html', context)
  