import datetime
from django.db import models

# Create your models here.

class Todo(models.Model):
    titel = models.CharField(max_length=200)
    erledigt = models.BooleanField(default=False)
    datum = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.titel